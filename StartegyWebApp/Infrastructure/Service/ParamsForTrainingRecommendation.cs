﻿namespace StrategyWebApp.Infrastructure.Service
{
    public class ParamsForTrainingRecommendation : IParamsForTrainingRecommendation
    {
        public WeatherTypeEnum WeatherType { get; set; }
    }
}
