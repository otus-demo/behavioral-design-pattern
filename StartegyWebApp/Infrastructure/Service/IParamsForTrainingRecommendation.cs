﻿namespace StrategyWebApp.Infrastructure.Service
{
    public interface IParamsForTrainingRecommendation
    {
        public WeatherTypeEnum WeatherType { get; set; }
    }
}
