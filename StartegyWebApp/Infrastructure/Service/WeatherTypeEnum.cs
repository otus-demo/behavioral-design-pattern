﻿namespace StrategyWebApp.Infrastructure.Service
{
    public enum WeatherTypeEnum
    {
        Cold,
        Warm,
        Hot
    }
}
