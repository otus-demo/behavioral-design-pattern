﻿namespace StrategyWebApp.Infrastructure.Core.Abstraction
{
    public interface ITrainingType
    {
        int Id { get; set; }
        string Name { get; set; }
    }
}
