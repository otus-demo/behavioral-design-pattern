
using Microsoft.OpenApi.Models;
using StrategyWebApp.Infrastructure.Core.Abstraction;
using StrategyWebApp.Infrastructure.DataAccess.Data;
using StrategyWebApp.Infrastructure.Service;
using System.Reflection;

namespace StartegyWebApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var builder = WebApplication.CreateBuilder(args);

            // Add services to the container.

            builder.Services.AddControllers();
            // Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
            builder.Services.AddEndpointsApiExplorer();
            builder.Services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Version = "v1",
                    Title = "Strategy web app",
                    Description = "Behavioral design patterns demo",
                    
                });

                // using System.Reflection;
                var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
            });

            builder.Services.AddSingleton<IDataContext<IWeatherForecast>,WeatherForecastDataContext > ();
            builder.Services.AddTransient<ITrainingRecommendationStrategy, FirstTrainingRecommendationStrategy>();
            builder.Services.AddTransient<ITrainingRecommendationStrategy, SecondTrainingRecommendationStrategy>();
            builder.Services.AddTransient<IContextForTrainingRecommendation, ContextForTrainingRecommendation>();


            builder.Logging.ClearProviders();
           // builder.Logging.AddConsole();
            builder.Logging.AddDebug();
            builder.Logging.AddEventLog();
            builder.Logging.SetMinimumLevel(LogLevel.Information);

            var app = builder.Build();

            // Configure the HTTP request pipeline.
            if (app.Environment.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseAuthorization();


            app.MapControllers();

            app.Run();
        }
    }
}